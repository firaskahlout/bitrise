//
//  ViewController.swift
//  Test
//
//  Created by Firas Alkahlout on 8/17/20.
//  Copyright © 2020 Firas Alkahlout. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var label: UILabel!
    
    private let text = """
    Legend:\n                (*) - local vPC is down, forwarding via vPC peer-link\n\nvPC domain id                     : 99  \nPeer status                       : peer link is down             \nvPC keep-alive status             : peer is alive                 \nConfiguration consistency status  : failed  \nPer-vlan consistency status       : success                       \nConfiguration inconsistency reason: Consistency Check Not Performed\nType-2 inconsistency reason       : Consistency Check Not Performed\nvPC role                          : none established              \nNumber of vPCs configured         : 2   \nPeer Gateway                      : Enabled\nDual-active excluded VLANs        : -\nGraceful Consistency Check        : Disabled (due to peer configuration)\nAuto-recovery status              : Disabled\nDelay-restore status              : Timer is off.(timeout = 30s)\nDelay-restore SVI status          : Timer is off.(timeout = 10s)\n\nvPC Peer-link status\n---------------------------------------------------------------------\nid   Port   Status Active vlans    \n--   ----   ------ --------------------------------------------------\n1    Po99   down   -                                                      \n\nvPC status\n----------------------------------------------------------------------\nid   Port   Status Consistency Reason                     Active vlans\n--   ----   ------ ----------- ------                     ------------\n5    Po5    down   Not         Consistency Check Not      -               \n                   Applicable  Performed                                  \n20   Po12   down   Not         Consistency Check Not      -               \n                   Applicable  Performed
    """
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(text)
        view.backgroundColor = .black
        label.text = text.description
        label.backgroundColor = .black
        label.textColor = .white
        label.coordinateSpace.isProxy()
    }

}
